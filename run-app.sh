#!/bin/bash

app_img="anilraje122/myapp"

# Install docker if it is not present in the system
if [[ $(which docker) && $(docker -v)  ]]; then
  echo -e "\nDocker is present in the system.."
else
  echo -e "\nWarning: Docker not found! Installing Docker.."
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh
fi

# Pull the above docker image on the host
echo -e "\nPulling the docker image.."
docker pull $app_img

# Shutdown container if running  
container=$(docker ps -a -q --filter=name=myapp)
if [[ -n $container ]]; then   
  echo -e "\nStop and removing the running container.."  
  docker stop $container && docker rm $container
fi

# Restart with the version of the image downloaded in step above
echo -e "\nRestarting app with latest image version"
docker run -d --name myapp -p 8080:8080 $app_img

# Verify that the container started successfully
if [[ -n $(docker ps -a -q --filter=name=myapp) ]]; then
  echo -e "\nContainer restarted successfully!"
else
  echo -e "\nError: Container restart failed!"  
fi

# Waiting for application to come up
echo -e "\nWating for application to come up.."
sleep 10

# Verify application is running successfully
if [[ "$(curl -Is "localhost:8080" | head -n 1)" == *"200 OK"* ]]; then
  echo -e "\nApplication is up and running on localhost:8080\n"
else
  echo -e "\nError: Application is not running!\n"
fi